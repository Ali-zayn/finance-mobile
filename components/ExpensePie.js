import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Image, ScrollView, Text } from 'react-native';
import { VictoryPie } from "victory-native";

function ExpensePie() {
    const graphicData = [{x: 'cats', y: 100},{x: "Dogs", y: 90},{x: "birds", y: 250}];
    const graphicColor = ['rgb(255, 0, 0)', 'rgb(220, 0, 0)', 'rgb(185, 0, 0)'];
  return (
    <VictoryPie
    data={graphicData}
    width={250}
    height={250}
    innerRadius={50}
    style={{labels: {fill: 'black', fontSize: 13, padding: 12}}}
    colorScale={graphicColor}
    /> 
  )
}

export default ExpensePie