import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Image, ScrollView, Text } from 'react-native';
import IncomePie from './IncomePie';
import ExpensePie from './ExpensePie';

export default function Home() {
  return (
    <ScrollView style={{ flex: 1 }}>
      <View style={styles.container}>
      <IncomePie />
      <Text style={{position: 'absolute',top: 110,left:'38%',fontSize: 20}}> Incomes </Text>
      <ExpensePie />
      <Text style={{position: 'absolute',top: 360,left:'37%',fontSize: 20}}> Expenses </Text>
      </View>
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: "#f5fcff"
  }
});