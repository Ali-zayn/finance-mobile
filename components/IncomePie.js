import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Image, ScrollView, Text } from 'react-native';
import { VictoryPie } from "victory-native";

function IncomePie() {
    const graphicData = [{x: 'Sales', y: 10},{x: "Dogs", y: 90},{x: "birds", y: 50}];
    const graphicColor = ['rgb(50, 50, 255)', 'rgb(50, 50, 220)', 'rgb(50, 50, 185)'];
  return (
    <VictoryPie
    data={graphicData}
    width={250}
    height={250}
    innerRadius={50}
    style={{labels: {fill: 'black', fontSize: 13, padding: 12}}}
    colorScale={graphicColor}
    /> 
  )
}

export default IncomePie