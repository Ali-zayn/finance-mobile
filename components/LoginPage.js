import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Image, TouchableOpacity, Text } from 'react-native';

export default function LoginPage({navigation}) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [data, setData] = useState(null);

  const onSubmit = () => {
    fetch(`https://codi-finance-app.herokuapp.com/api/adminLogin?username=${username}&password=${password}`, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },

    })
        .then((reponse) => {
            if (reponse.ok) {
                return reponse.json();
            }
            throw reponse;
        })
        .then((data) => {
            setData(data);
            console.log(data);
            //
            // if(data.length === 0){
            //     return alert("incorrect username and password");
            // }
            if (data.status.length === 0) {
                return alert("incorrect username and password");
            }
             else if (data.status[0].isActive === true){
                navigation.navigate("Reports");
            } else if( data.status[0].isActive === false){
                return alert("inactive Admin");
            }

        }, );
};
  return (
    <View style={styles.container}>
      <Image style={styles.image} source={require("../assets/logo192.png")} />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Username"
          placeholderTextColor="#003f5c"
          onChangeText={(email) => setUsername(email)}
        />
      </View>

      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#003f5c"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <TouchableOpacity onPress={()=>{onSubmit();}} style={styles.loginBtn}>
        <Text style={styles.loginText}>LOGIN</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  image: {
    marginBottom: 40
  },
  inputView: {
    backgroundColor: "#eee",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
  },

  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },
  loginBtn:
  {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "rgb(68, 134, 255)",
  },
  loginText: {
      color: "#FFF",
  }

});
