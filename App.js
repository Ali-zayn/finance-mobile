import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, TextInput, View, Image, TouchableOpacity, Text } from 'react-native';
import LoginPage from "./components/LoginPage";
import Home from './components/Home';
import { Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
  return (
    <Provider>
      <NavigationContainer>
        <Stack.Navigator
         screenOptions={{
        headerStyle: { backgroundColor: "rgb(68, 134, 255)" },
        headerTintColor: 'white',
        contentStyle: { backgroundColor: "rgb(68, 134, 255)" },
      }}
       initialRouteName="StartScreen"
       >
          <Stack.Screen name="Login" component={LoginPage} />
          <Stack.Screen name="Reports" component={Home} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
